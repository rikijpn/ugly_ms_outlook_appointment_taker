#!/usr/bin/env python

# uses python3 btw

"""
Updates your refresh token in your config.yml file.
You'll need to have an initial configuration. Refer to sample_requests.txt
in order to get the necessary initial tokens.
"""

import yaml
import requests
import json

def get_config():
    with open("config.yml", 'r') as stream:
        saved_config = yaml.safe_load(stream)
    return saved_config


def update_config(new_config):
    with open("config.yml", 'w') as stream:
        yaml.dump(new_config, stream)


def refresh_token():
    "update the token that only lasts like one hour"
    url = "https://login.microsoftonline.com/{}/oauth2/v2.0/token".format(
        saved_config['tenant_id'])
    headers = {'Content-Type': 'application/x-www-form-urlencoded'}
    data_list = [ 'client_id={}'.format(saved_config['client_id']),
                  'scope=https%3A%2F%2Fgraph.microsoft.com%2F.default',
                  'refresh_token={}'.format(saved_config['refresh_token']),
                  'grant_type=refresh_token'
    ]
    data = '&'.join(data_list)
    request = requests.post(url, headers=headers, data=data)
    results = json.loads(request.content.decode('utf-8'))
    new_config = saved_config
    new_config['access_token'] = results['access_token']
    new_config['refresh_token'] = results['refresh_token']
    return new_config


def test_token_works(config):
    url = "https://graph.microsoft.com/v1.0/me"
    headers = {'Authorization': 'Bearer {}'.format(config['access_token'])}
    request = requests.get(url, headers=headers)
    results = json.loads(request.content.decode('utf-8'))
    return results


if __name__ == "__main__":
    saved_config = get_config()
    new_config = refresh_token()
    # if you ever feel like testing it without restclient for some reason
    # test_token_works(new_config)

    update_config(new_config)
