#!/usr/bin/env python

import yaml
import requests
import json
import datetime
from datetime import timedelta
from copy import deepcopy
from multiprocessing import Pool

import pprint


def get_config(file_name):
    with open(file_name, 'r') as stream:
        config = yaml.safe_load(stream)
    return config


def make_holidays_list():
    "hey, it's one way to do it"
    holidays_raw = [
        '2020-01-01',
        '2020-01-13',
        '2020-02-11',
        '2020-02-23',
        '2020-02-24',
        '2020-03-20',
        '2020-04-29',
        '2020-05-03',
        '2020-05-04',
        '2020-05-04',
        '2020-05-05',
        '2020-07-20',
        '2020-08-11',
        '2020-09-21',
        '2020-09-22',
        '2020-10-12',
        '2020-11-03',
        '2020-11-23',
    ]
    holidays_dates = []
    for h in holidays_raw:
        holidays_dates.append(
            datetime.date(*list(map(lambda d: int(d), (h.split('-')))))
            )
    return holidays_dates


def get_next_day(days_delta=30):
    "get the day for the next possible day to book appointment"
    today = datetime.date.today()
    number_of_days_next = timedelta(days=days_delta)
    target_day = today + number_of_days_next

    holidays = make_holidays_list()

    if target_day.weekday() < 5:
        next_day_return = {}
        next_day_return['date'] = target_day.strftime("%Y-%m-%d")
        next_day_return['dow'] = target_day.weekday()
        return next_day_return
    else:
        return "n/a"


def add_time_to_request(outlook_request_orig, day, time_slot):
    outlook_request = {}
    outlook_request = {k: v for k, v in outlook_request_orig.items()}
    outlook_request['start'] = {}
    outlook_request['end'] = {}
    outlook_request['start']['dateTime'] = '{}T{}:00'.format(
        day, time_slot['start'])
    outlook_request['start']['timeZone'] = 'Asia/Tokyo'
    outlook_request['end']['dateTime'] = '{}T{}:00'.format(
        day, time_slot['end'])
    outlook_request['end']['timeZone'] = 'Asia/Tokyo'

    # extra
    # delete rikijpn key
    del outlook_request['rikijpn_dow']

    return outlook_request


def add_time_to_requests(outlook_config_complete, day, time_slot):
    """
    They only allow us to book a meeting room for max of 2 hours.
    This would book one from 09:00 to 19:00 on regular days.
    """
    avail_time_slots = {}
    avail_time_slots['whole_day'] = [
            {'start': '09:00', 'end': '11:00'},
            {'start': '11:00', 'end': '13:00'},
            {'start': '13:00', 'end': '15:00'},
            {'start': '15:00', 'end': '17:00'},
            {'start': '17:00', 'end': '19:00'},
    ]
    avail_time_slots['no_morning'] = [
            {'start': '11:00', 'end': '13:00'},
            {'start': '13:00', 'end': '15:00'},
            {'start': '15:00', 'end': '17:00'},
            {'start': '17:00', 'end': '19:00'},
    ]
    avail_time_slots['only_morning'] = [
            {'start': '09:00', 'end': '11:00'},
    ]

    time_slots = avail_time_slots[time_slot]

    results = []
    for outlook_conf in outlook_config_complete:
        # match only when dow matches list's
        if outlook_conf['rikijpn_dow'] != day['dow']:
            continue

        for time_slot in time_slots:
            new_appointment = {}
            new_appointment = add_time_to_request(
                outlook_conf.copy(), day['date'], time_slot.copy()).copy()
            results = results + [deepcopy(new_appointment)]
    return results


def book_meeting_room(appointment_request):
    url = "https://graph.microsoft.com/v1.0/me/events"
    headers = {'Authorization': "Bearer {}".format(api_conf['access_token']),
               'Content-Type': 'application/json',
               'Prefer': 'outlook.timezone="Asia/Tokyo"'
               }
    data = json.dumps(appointment_request)
    request = requests.post(url, headers=headers, data=data)
    results = json.loads(request.content.decode('utf-8'))
    return results


def print_multi_process_results(mp_results):
    for result in mp_results:
        print("=" * 80)
        pprint.pprint(result.get())


def generate_appointments_based_on_day(appointment_conf, day, day_relation):
    """
    runs add_time_to_request according to the day of the week value
    day_relation shows whether the day running is for the same day of the week
    (Monday -> Monday) or the day after that (Mon -> Tue, as for next day's
    morning reservations.
    """

    appointment = []

    if day == 'n/a':
        return []

    if day_relation == 'same':
        if day['dow'] == 0:  # on Mondays, get the whole day
            appointment = add_time_to_requests(
                appointment_conf, day, 'whole_day')
        else:
            appointment = add_time_to_requests(
                appointment_conf, day, 'no_morning')
    else:
        appointment = add_time_to_requests(
            appointment_conf, day, 'only_morning')

    return appointment


if __name__ == "__main__":
    next_day = get_next_day()
    next_next_day = get_next_day(30)
    appointment_conf = get_config('appointment_requests_daily.yml')
    api_conf = get_config('config.yml')
    next_appointment_requests_yaml = generate_appointments_based_on_day(
        appointment_conf, next_day, 'same')
    next_next_appointment_requests_yaml = generate_appointments_based_on_day(
        appointment_conf, next_next_day, 'next_day')
    appointment_requests_yaml = next_appointment_requests_yaml + \
        next_next_appointment_requests_yaml

    if appointment_requests_yaml == []:
        print("nothing to do")
        exit(0)

    pprint.pprint(appointment_requests_yaml)

    requests_pool = Pool(processes=4)
    results = []
    for appointment in appointment_requests_yaml:
        request_result = requests_pool.apply_async(
            book_meeting_room, (appointment,))
        results.append(request_result)
    requests_pool.close()
    requests_pool.join()

    print_multi_process_results(results)
