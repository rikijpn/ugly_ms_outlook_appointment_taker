# What's this?
Just using the damn ms graph api thingy to get meeting rooms periodically. As my company, sadly uses microsoft outlook (office365), and limit the days you can book them in advance. This was the only thing I could do besides setting up the meeting manually by outlook.  
Details in blogspot's blog: [using microsoft graph api to book stuff](https://rikijpn.blogspot.com/2020/03/using-microsoft-graph-api-to-book.html)

It basically has a list of meeting rooms, and tries to book each on the list for the whole day. You can change `add_time_to_requests`'s times to change that. Oh, and everything is with Asia/Tokyo timezone, you might want to change that too.  
I also have a `rikijpn_dow` variable, so I can pick different meeting rooms based on the day.  
I'm actually super mean, and abuse the system by using the multiprocess library, to send like 4 requests at the same time (if I sent more the server rejected them...), and each request is booking multiple meeting rooms. Your organization might not like you doing that. But again, your organization is making you use f*cking ms office, so whose fault is it really? I'm so philosophical sometimes.

# Preparation (create an "app")
1. create app (in your azure portal)
  In the left-hand navigation pane, select the Azure Active Directory service, and then select App registrations → New registration.
  https://portal.azure.com/#blade/Microsoft_AAD_IAM/ActiveDirectoryMenuBlade/RegisteredApps
2. in azure, select Manage → API Permissions → app (Microsoft APIs → Microsoft Graph → Delegated permissions)
  Make sure you have the following permissions:
```
Calendars.ReadWrite
User.Read
offline_access
```
3. in azure, select Manage → Authentication → "Default client type" → "Treat application as public client"
4. in azure, select Manage → Authentication → "Supported account types" → "Accounts in this organizational directory only (this will probably have your company/school name here)"
5. get consent
```
https://login.microsoftonline.com/${tenant_id}/oauth2/v2.0/authorize?client_id=${client_id}&amp;response_type=code&amp;response_mode=query&amp;state=12345&amp;scope=offline_access%20user.read%20Calendars.ReadWrite
```
  The "tenant_id" here is just  whatever you already have in your url when looking azure, a long senseless string, that is common through all the pages for your organization.  
  "client_id" will be shown in your "app"'s top/info page.  
  You can just ignore the rest and use it as it is.  
  Put this in your browser, and you'll be sent to a blank page by default. Check the URL in your browser, and you'll see there will be an "access" and "refresh" token.

# Basic usage and test directly without using the script
Check [sample requests](sample_requests.txt)

# About the code here
## Files
1. appointment_requests_daily.yml  
   Contains the list of meeting rooms, and the day of the week you want to use them.
2. config_sample.yml  
   Sample to help you make config.yml.
3. get_meeting_rooms.py  
   Script that actually sends the appointments.
4. get_token.py  
   Scripts to update your tokens. It should be run a couple of mins before `get_meeting_rooms.py`. It'll update your access token, and also keep some nice DNS cache, so the actual `get_meeting_rooms.py` script will run as fast/smooth as possible.
5. sample_requests.txt
   emacs restclient mode file to use to get the tokens first time you set up the "app", and when you change your password (you have to "grant" again everytime you do).

   
